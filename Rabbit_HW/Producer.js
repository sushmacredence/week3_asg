var amqp = require('amqplib/callback_api');
//Establishing Connection with RabbitMQ server
amqp.connect('amqp://localhost', function(error0, connection) {
  if (error0) {
    throw error0;
  }
  //creating a channel
  connection.createChannel(function(error1, channel) {
      if(error1){
        throw error1
      }
      const queue ='hello'  //declaring a queue
      var msg = 'hello sushma'
      channel.assertQueue(queue,{
          durable:false
        })
      channel.sendToQueue(queue,Buffer.from(msg));
      console.log(" [x] sent %s", msg)
  });
setTimeout(function() { 
    connection.close(); 
    process.exit(0) 
    }, 500);
});