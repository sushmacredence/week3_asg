module.exports = {
    "client_doc_create": async (obj) => {
        try {
            let result, insResult
            let rQry = [], insQry = []
            let updateOraDB = funclib['updateOraDB']
            rQry[rQry.length] = `SELECT distinct dm.doc_code FROM wm_document_list dm,(SELECT dc.doc_code FROM wm_document_master dc, wm_client_master wcm WHERE dc.category_code =`
            rQry[rQry.length] = `wcm.category_code AND Upper(dc.nationality)=Upper(wcm.nationality) AND wcm.client_id='${obj.data.client_id}' UNION ALL SELECT dc.doc_code FROM wm_document_master dc,`
            rQry[rQry.length] = `wm_client_master wcm WHERE dc.category_code = wcm.category_code AND Upper(dc.nationality)='ALL' AND wcm.client_id='${obj.data.client_id}') cd WHERE dm.doc_code = cd.doc_code`
            rQry[rQry.length] = `AND authorization_status = 1 AND deleted = 'N'`
            result = await updateOraDB('iwf', rQry.join(" "))
            if (!result.data || !result.data.rows || result.data.rows.length == 0) {
                logger.debug(`No Records found for ${obj.data.client_id}`)
                return `{"status":"unsuccess","msg":"No data found for ${obj.data.client_id}"}`
            }
            insQry[insQry.length] = `DELETE from wm_client_documents WHERE ref_id='${obj.data.client_id}';`
            for (let k in result.data.rows) {
                logger.info(result.data.rows[k])
                insQry[insQry.length] = `INSERT INTO wm_client_documents (srno, doc_code, receive_status, ref_id) VALUES (doc_srno_seq.NEXTVAL,'${result.data.rows[k]}','P','${obj.data.client_id}');`
            }
            insQry[insQry.length] = `INSERT INTO audit_wm_client_documents SELECT a.*,'create','${obj.data.user_id}',sysdate,'create','1' FROM wm_client_documents a WHERE ref_id='${obj.data.client_id}';`
            insQry = "BEGIN " + insQry.join(" ") + " END;"
            logger.debug(insQry)
            insResult = await updateOraDB('iwf', insQry)
            if (insResult.status) {
                logger.info("Client documents created successfully.");
                return '{"status":"success","msg":"Client documents created successfully.","error":""}';
            }
        }
        catch (error) {
            logger.error(error)
            return '{"status":"unsuccess","msg":"","error":"Error Occurred ' + error + '"}';
        }
    },
    "client_acc_doc_create": async (obj) => {
        try {
            let rQry = [], insQry = []
            let result, insResult
            let updateOraDB = funclib['updateOraDB']
            rQry[rQry.length] = `SELECT distinct dm.doc_code FROM wm_document_list dm,(`
            rQry[rQry.length] = `SELECT dc.doc_code FROM wm_acc_document_master dc, wm_client_ac_master wca`
            rQry[rQry.length] = `WHERE dc.acct_type = wca.acct_type AND Upper(dc.sub_ac_type)=Upper(wca.sub_ac_type) AND wca.pms_acno='${obj.data.client_account_id}'`
            rQry[rQry.length] = `UNION ALL SELECT dc.doc_code FROM wm_acc_document_master dc, wm_client_ac_master wca`
            rQry[rQry.length] = `WHERE dc.acct_type = wca.acct_type AND Upper(dc.sub_ac_type)='ALL' AND wca.pms_acno='${obj.data.client_account_id}'`
            rQry[rQry.length] = `) cd WHERE dm.doc_code = cd.doc_code AND authorization_status = 1 AND deleted = 'N'`
            result = await updateOraDB('iwf', rQry.join(" "))
            if (!result.data || !result.data.rows || result.data.rows.length == 0) {
                logger.debug(`No Records found for ${obj.data.client_account_id}`)
                return `{"status":"unsuccess","msg":"No data found for ${obj.data.client_account_id}"}`
            }
            insQry[insQry.length] = `DELETE from wm_client_ac_documents WHERE ref_id='${obj.data.client_account_id}';`
            for (let k in result.data.rows) {
                logger.info(result.data.rows[k])
                insQry[insQry.length] = `INSERT INTO wm_client_ac_documents (srno, doc_code, receive_status, ref_id) VALUES (doc_acc_srno_seq.NEXTVAL,'${result.data.rows[k]}','P','${obj.data.client_account_id}');`
            }
            insQry[insQry.length] = `INSERT INTO audit_wm_client_ac_documents SELECT a.*,'create','${obj.data.user_id}',sysdate,'create','1' FROM wm_client_ac_documents a WHERE ref_id='${obj.data.client_account_id}';`
            insQry = "BEGIN " + insQry.join(" ") + " END;"
            logger.debug(insQry)
            insResult = await updateOraDB('iwf', insQry)
            if (insResult.status) {
                logger.info("Client account documents created successfully.");
                return '{"status":"success","msg":"Client account documents created successfully.","error":""}';
            }
        }
        catch (error) {
            logger.error(error)
            return '{"status":"unsuccess","msg":"","error":"Error Occurred ' + error + '"}';
        }
    },
    "order_doc_create": async (obj) => {
        try {
            let rQry = [], insQry = []
            let result, insResult
            let updateOraDB = funclib['updateOraDB']

            // read qry of order mandate
            rQry[rQry.length] = `SELECT tdm.doc_code FROM eq_preorder  eqp,if_vw_port_cl vwp,wm_trans_document_type tdm`
            rQry[rQry.length] = `WHERE eqp.preorder_id='${obj.data.preorder_id}' AND eqp.in_portcode=vwp.client_account_id AND vwp.category_code=tdm.client_category`
            rQry[rQry.length] = `AND vwp.acct_type=tdm.account_type AND vwp.sub_ac_type=tdm.sub_account_type AND eqp.ordertype=tdm.transaction_type`

            result = await updateOraDB('iwf', rQry.join(" "))
            if (!result.data || !result.data.rows || result.data.rows.length == 0) {
                logger.debug(`No Records found for ${obj.data.preorder_id}`)
                return `{"status":"unsuccess","msg":"No data found for ${obj.data.preorder_id}"}`
            }
            insQry[insQry.length] = `DELETE from wm_trans_documents WHERE ref_id='O${obj.data.preorder_id}';`
            for (let k in result.data.rows) {
                logger.info(result.data.rows[k])
                insQry[insQry.length] = `INSERT INTO wm_trans_documents (srno, doc_code, receive_status, ref_id) VALUES (trans_doc_srno_seq.NEXTVAL,'${result.data.rows[k]}','P','O${obj.data.preorder_id}');`
            }
            insQry[insQry.length] = `INSERT INTO audit_wm_trans_documents SELECT a.*,'create','${obj.data.user_id}',sysdate,'create','1' FROM wm_trans_documents a WHERE ref_id='O${obj.data.preorder_id}';`
            insQry = "BEGIN " + insQry.join(" ") + " END;"
            logger.debug(insQry)
            insResult = await updateOraDB('iwf', insQry)
            if (insResult.status) {
                logger.info("Order documents created successfully.");
                return '{"status":"success","msg":"Order documents created successfully.","error":""}';
            }
        }
        catch (error) {
            logger.error(error)
            return '{"status":"unsuccess","msg":"","error":"Error Occurred ' + error + '"}';
        }
    }
}
