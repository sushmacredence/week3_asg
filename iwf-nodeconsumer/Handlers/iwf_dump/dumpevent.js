const moment = require("moment");
const dateformat = (config.DateFormat) ? config.DateFormat : "DD/MM/YYYY"

module.exports = emitter => {
  emitter.on("iwfdump",async msg => {
    try{
      let mObj = msg.data
      let asondate = moment(mObj.asondate, dateformat);
      let updateOraDB =funclib['updateOraDB']
      let insertQry=[];
      let result = "";
      
      insertQry[insertQry.length] = `DELETE FROM portfolio_daily_position WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}'; INSERT INTO portfolio_daily_position select * from iwf_portfolio_daily_position WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}';`
      insertQry[insertQry.length] = `DELETE FROM data_dump_activity WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}'; INSERT INTO data_dump_activity select * from iwf_data_dump_activity WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}';`
      insertQry[insertQry.length] = `DELETE FROM portfolio_bnch_market_returns WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}'; INSERT INTO portfolio_bnch_market_returns select * from iwf_port_bnch_market_returns WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}';`
      insertQry[insertQry.length] = `DELETE FROM change_in_portvalue WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}'; INSERT INTO change_in_portvalue select * from iwf_change_in_portvalue WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}';`
      insertQry[insertQry.length] = `DELETE FROM nav_calculation_data WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}'; INSERT INTO nav_calculation_data select * from iwf_nav_calculation_data WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}';`
      insertQry[insertQry.length] = `DELETE FROM rm_clientdsdsa_count WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}'; INSERT INTO rm_client_count select * from iwf_rm_client_count WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}';`
      insertQry[insertQry.length] = `DELETE FROM rm_cust_analysis WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}'; INSERT INTO rm_cust_analysis select * from iwf_rm_cust_analysis WHERE Trunc(ds_generationdate)='${asondate.format(dateformat)}';`

      insertQry="BEGIN "+insertQry.join(" \n ")+" END;"
      result = await updateOraDB('iwf',insertQry);
      if(result.status)
        logger.info("IWF data dumped successfully.");
    }
    catch(error)
    {
      logger.warn(error);
    }
  });
};

let test_msg = {
  "eventname": "iwfdump",
  "data": {
    "asondate":"25-07-2019"
  }
}