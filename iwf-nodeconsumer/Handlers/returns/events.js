const path = require("path")
let fpath = require(path.join(__dirname, "../config.js"));
let { PythonShell } = require('python-shell')

module.exports = async function (handler) {
    // handler.on function registers the event handlers "csv" inside email consumer.
    handler.on("returns", async msg => {
        try {
            cred = {
                "oraUser": "STD_IWF_DEV",
                "oraPassword": "std_iwf_dev",
                "oraHost": "192.168.1.16",
                "oraPort": 1521,
                "oraInstance": "wforcl"
            }
            Object.assign(msg["data"],cred)
            let argument = JSON.stringify(msg["data"])
            let options = {
                mode: 'text',
                args: argument
            };

            console.log(options)
            console.log("Process Started");
            PythonShell.run('' + fpath["filepath"] + 'processUnit.py', options, function (err, res) {
                if (err) throw err;
                console.log(res);
                console.log("Process Completed");
                
            });

        } catch (error) {
            logger.error(`FileOperation: returns: Error: ${error}`);
        }
    })
}



Rab_msg = {
    "eventname": "returns",
    "user": {
        "user_id": "sagarsp",
        "sessionid": "237260429126477122008302166698",
        "app_id": "xyzdas123"
    },
    "data": {
        "event": "portsec",
        "date": "2018-01-01"
    }
}


