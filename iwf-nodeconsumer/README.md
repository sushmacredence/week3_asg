# Node Consumer

### Required node modules

`npm install`

### Config -> (Handlers/config.json file)

```json
{
  "applicationIp":"<JBOSS APP IP>",
  "rabbitmq": {
    "username": "username",
    "password": "password",
    "serverip": "<IP>"
  },
  "oracledb": [
    {
      "alias": "oraInsightsDev",
      "username": "username",
      "password": "password",
      "connectString": "<IP>:1521/<DB>"
    },
    {
      "alias": "vizhome",
      "user": "viz",
      "password": "vijay",
      "connectString": "<IP>:1521/<DB>"
    }
  ],
  "DateFormat": "DD/MM/YYYY",
  "mongodb1": [
    {
      "alias": "mongo",
      "connectString": "mongodb://<IP>:27017",
      "db": "<DBNAME>"
    }
  ],
  "notification": {
    "url": "https://<IP>:<PORT>/"
  }
}
```

### How to add new consumer

  ```
  Handlers
  ├── aggregates                  <---- Queuename Folder
  │   ├── countrytotals.js        <---- EventModule .js file
  │   └── exchangekeys.json
  ├── email                       <---- Queuename Folder
  │   ├── sendmail                <---- EventModule Folder
  │   │   └── index.js
  │   └── someOtherEvent.js       <---- EventModule .js file
  └── config.json
  ```

- Inside **Handlers** folder, create folder-name same as the queue-name you want to consume the messages for.
- For eg,
  - To create a consumer for **aggregates queue**, we will create aggregates folder inside Handlers.
  - Next, we need to create event modules which can be a **folder** or a **js file**,
    > *Note: if folder than index.js is must inside it.*
  - Templete for eventmodule - index.js

    ```js
        // export a function which accepts handler event from consumermanager
        module.exports = async function (handler) {
        try {
            // handler.on function registers the event handlers "sendMail" inside email consumer.
            handler.on("sendMail", function (msg) {
                logger.info(`inside Send mail : Msg : ${msg.data.message}`)
            })
            return null
        } catch (error) {
            logger.error(error)
            }
        }
    ```

  - message format for email queue

    ```json
      {
          "eventname": "sendMail",
          "data": {
              "message": "testing sendmail event"
          }
      }
    ```

  - Here email consumer will call the handler function sendMail which we register above
  - The messages are routed based on eventname. 

run the nodeConsumer server from cmd.  
`node index.js`
